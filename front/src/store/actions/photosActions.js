import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_PHOTOS_SUCCESS = "FETCH_PHOTOS_SUCCESS";
export const FETCH_PHOTOS_ERROR = "FETCH_PHOTOS_ERROR";

export const FETCH_USER_PHOTOS_SUCCESS = "FETCH_USER_PHOTOS_SUCCESS";
export const FETCH_USER_PHOTOS_ERROR = "FETCH_USER_PHOTOS_ERROR";

export const ADD_PHOTO_SUCCESS = 'ADD_PHOTO_SUCCESS';
export const ADD_PHOTO_ERROR = "ADD_PHOTO_ERROR";

export const DELETE_PHOTO_ERROR = "DELETE_PHOTO_ERROR";


const fetchPhotosSuccess = value => ({type: FETCH_PHOTOS_SUCCESS, value});
const fetchPhotosError = error => ({type: FETCH_PHOTOS_ERROR, error});

const fetchUserPhotosSuccess = value => ({type: FETCH_USER_PHOTOS_SUCCESS, value});
const fetchUserPhotosError = error => ({type: FETCH_USER_PHOTOS_ERROR, error});

const addPhotoSuccess = value => ({type: ADD_PHOTO_SUCCESS, value});
const addPhotoError = error => ({type: ADD_PHOTO_ERROR, error});

const deletePhotoError = error => ({type: DELETE_PHOTO_ERROR, error});

export const fetchPhotos = () => {
    return async dispatch => {
        try {
            const response = await axiosApi.get("photos");
            dispatch(fetchPhotosSuccess(response.data));
        } catch (e) {
            dispatch(fetchPhotosError(e));
        }
    };
};



export const addPhoto = data => {
    return async dispatch => {
        try {
            await axiosApi.post('/photos', data);
            dispatch(addPhotoSuccess());
            dispatch(historyPush("/"));
        } catch (e) {
            dispatch(addPhotoError(e.response.data))
        }
    };
};



export const fetchUserPhotos = id => {
    return async dispatch => {
        try {
            const response = await axiosApi.get("photos?user=" + id);
            dispatch(fetchUserPhotosSuccess(response.data));
        } catch (e) {
            dispatch(fetchUserPhotosError(e));
        }
    };
};



export const deletePhoto = id => {
    return async dispatch => {
        try {
            await axiosApi.delete('/photos/' + id);
            dispatch(historyPush("/"));
        } catch (e) {
            dispatch(deletePhotoError(e));
        }
    };
}