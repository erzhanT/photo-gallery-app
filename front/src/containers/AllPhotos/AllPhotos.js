import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPhotos} from '../../store/actions/photosActions'
import {makeStyles} from '@material-ui/core/styles';
import Container from "@material-ui/core/Container";
import OnePhoto from "../../components/SinglePhoto/OnePhoto";

const useStyles = makeStyles(() => ({
    main: {
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-between",
        paddingTop: "40px"
    },
}));

const AllPhotos = () => {
    const classes = useStyles();
    const photos = useSelector(state => state.photos.photos);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchPhotos());
    }, [dispatch]);


    return (
        <Container className={classes.main}>
            {photos.map(photo => {
                return (
                    <OnePhoto
                        key={photo._id}
                        title={photo.title}
                        src={'http://localhost:8000/uploads/' + photo.image}
                        author={photo.user.displayName}
                        user={photo.user._id}
                        permit={false}
                    />
                )
            })}
        </Container>
    );
};

export default AllPhotos;