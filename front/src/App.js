import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import {useSelector} from "react-redux";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./components/Layout/Layout";
import AllPhotos from "./containers/AllPhotos/AllPhotos";
import MyPhotos from "./containers/MyPhotos/MyPhotos";
import AddPhoto from "./containers/AddPhoto/AddPhoto";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
    return isAllowed ?
        <Route {...props} /> :
        <Redirect to={redirectTo}/>;
};

const App = () => {

    const user = useSelector(state => state.users.user);

    return (
        <div className="App">

            <Layout/>
            <ProtectedRoute
                path="/signup"
                exact
                component={Register}
                isAllowed={!user}
            />
            <Switch>
                <Route path="/register" component={Register}/>
                <Route path="/login" component={Login}/>
                <Route path="/" exact component={AllPhotos}/>
                <Route path="/users/:id" component={MyPhotos} />
                <Route path="/add_photo" component={AddPhoto} />
            </Switch>
        </div>
    );
};

export default App;
