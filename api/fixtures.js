const mongoose = require('mongoose');
const config = require('./config');

const User = require("./models/User");
const Photo = require("./models/Photo");
const {nanoid} = require('nanoid');

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }
    const [user, user1] = await User.create({
        email: "qwer@qwer.qwer",
        password: "312",
        token: nanoid(),
        displayName: "Moprhling",

    }, {
        email: "zxc@zxc.zxc",
        password: "123",
        token: nanoid(),
        displayName: "Shadow",
        avatarImage: "avatar.png"
    });
    await Photo.create({
            title: "Tanjiro",
            image: "1.jpeg",
            user: user._id
        }, {
            title: "Samurai",
            image: "2.jpeg",
            user: user._id
        }, {
            title: "Nature",
            image: "3.jpg",
            user: user1._id
        },
        {
            title: "Brothers",
            image: "4.jpeg",
            user: user._id
        }, {
            title: "25th Baam",
            image: "5.jpeg",
            user: user1._id
        });

    await mongoose.connection.close();

}

run().catch(console.error);