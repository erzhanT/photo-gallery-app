const path = require('path');
const express = require('express');
const config = require("../config");
const Photo = require('../models/Photo');
const auth = require('../middleware/auth');
const {nanoid} = require("nanoid");
const multer = require("multer");

const router = express.Router();


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, path.join(config.uploadPath));
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});


router.get('/', async (req, res) => {
    let query;
    if (req.query.user) {
        query = {user: req.query.user}
    }
    const photos = await Photo.find(query).populate({path: "user"});
    if (photos) {
        res.send(photos);
    } else {
        res.sendStatus(404);
    }
});

router.post('/', auth, upload.single("image"), async (req, res) => {
    const photoData = req.body;
    photoData.user = req.user._id;
    if (req.file) {
        photoData.image = req.file.filename;
    }
    const photo = new Photo(photoData)
    try {
        await photo.save();
        res.send(photo);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.delete('/:id', auth, async (req, res) => {
    const photo = await Photo.findById({_id: req.params.id});
    if (photo.user.equals(req.user._id)) {
        const remove = await Photo.findByIdAndRemove({_id: req.params.id});
        if (remove) {
            res.send("Photo removed");
        } else {
            res.sendStatus(404);
        }
    } else {
        res.status(401).send({message: 'Access denied'});
    }

});

module.exports = router;

