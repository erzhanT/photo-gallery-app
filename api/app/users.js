const express = require('express');
const User = require('../models/User');
const config = require('../config');
const axios = require("axios");
const path = require("path");
const multer = require("multer");
const {nanoid} = require("nanoid");

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null,  path.join(config.uploadPath));
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});
const upload = multer({storage});


router.post('/', upload.single('avatarImage'), async (req, res) => {
    const userData = {
        displayName: req.body.displayName,
        email: req.body.email,
        password: req.body.password,
    }
    if (req.file) {
        userData.avatarImage = req.file.filename;
    }
    const user = new User(userData);
    try {
        user.generateToken();
        await user.save();
        res.send(user);
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.post('/sessions', async (req, res) => {
    const user = await User.findOne({email: req.body.email});

    if (!user) {
        return res.status(401).send({message: 'Credentials are wrong'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(401).send({message: 'Credentials are wrong'});
    }

    user.generateToken();
    await user.save();

    return res.send(user);
});

router.delete('/sessions', async (req, res) => {
    const token = req.get('Authorization');
    const success = {message: 'Success'};

    if (!token) return res.send(success);

    const user = await User.findOne({token});

    if (!user) return res.send(success);

    user.generateToken();

    await user.save();

    return res.send(success);
});

router.post('/facebookLogin', async (req, res) => {
    console.log(req.body);

    const inputToken = req.body.accessToken;

    const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;

    const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

    try {
        const response = await axios.get(debugTokenUrl);

        if (response.data.data.error) {
            console.log(response.data);
            return res.status(401).send({global: 'Facebook token incorrect'});
        }

        console.log(response.data);

        if (response.data.data['user_id'] !== req.body.id) {
            return res.status(401).send({global: 'User ID incorrect'});
        }

        let user = await User.findOne({email: req.body.email});

        if (!user) {
            user = await User.findOne({facebookId: req.body.id});
        }

        if (!user) {
            user = new User({
                email: req.body.email || nanoid(),
                password: nanoid(),
                facebookId: req.body.id,
                displayName: req.body.name,
                fbAvatar: req.body.picture.data.url
            });
        }

        user.generateToken();
        await user.save();
        res.send(user);
    } catch (e) {
        console.error(e);
        return res.status(401).send({global: 'Facebook token incorrect'})
    }
});


module.exports = router;